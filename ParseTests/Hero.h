//
//  Hero.h
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface Hero : NSObject

@property (strong, nonatomic) NSString *heroName;
@property (strong, nonatomic) NSString *heroPower;
@property (strong, nonatomic) UIImage *heroImage;
@property (strong, nonatomic) PFFile *heroImageToDownload;

+(Hero *)createHeroWithName:(NSString *)heroName andPower:(NSString *)heroPower andImageToDownloadLater:(PFFile *)imageToDownload;

@end
