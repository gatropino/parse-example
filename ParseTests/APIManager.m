//
//  APIManager.m
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import "APIManager.h"
#import <Parse/Parse.h>
#import "DataProcessor.h"

@implementation APIManager

+(void)retrieveObjectsOfCLass:(NSString *)classToSearchFor withSuccess:(completionBlock) blockCompleted
{
    NSMutableArray *objectsRetrieved = [[NSMutableArray alloc] init];
    
    PFQuery *query = [PFQuery queryWithClassName:classToSearchFor];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error)
    {
        if (!error)
        {
            for (PFObject *object in objects)
            {
                Hero *heroConverted = [DataProcessor convertPFObjectIntoHero:object];
                
                [objectsRetrieved addObject:heroConverted];
            }
            blockCompleted(true, objectsRetrieved);
        }
        else
        {
            blockCompleted(false, objectsRetrieved);
        }
    }];
    return;
}

+(void)retrieveImageForHero:(Hero *)heroNeedingImage
{
    [heroNeedingImage.heroImageToDownload getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error)
     {
         if (!error)
         {
             heroNeedingImage.heroImage = [UIImage imageWithData:data];
         }
         [[NSNotificationCenter defaultCenter] postNotificationName:@"imageDownloaded" object:heroNeedingImage];
     }];
}

@end
