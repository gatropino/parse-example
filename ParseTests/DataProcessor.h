//
//  DataProcessor.h
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero.h"
#import <Parse/Parse.h>

@interface DataProcessor : NSObject

+(Hero *)convertPFObjectIntoHero:(PFObject *)objectToProcess;

@end
