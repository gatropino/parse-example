//
//  APIManager.h
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero.h"

@interface APIManager : NSObject

typedef void(^completionBlock)(BOOL success, NSMutableArray *processedHeroes);

+(void)retrieveObjectsOfCLass:(NSString *)classToSearchFor withSuccess:(completionBlock) success;
+(void)retrieveImageForHero:(Hero *)heroNeedingImage;

@end
