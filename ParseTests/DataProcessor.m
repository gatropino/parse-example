//
//  DataProcessor.m
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import "DataProcessor.h"

@implementation DataProcessor

+(Hero *)convertPFObjectIntoHero:(PFObject *)objectToProcess
{    
    return [Hero
            createHeroWithName:objectToProcess[@"nameOfHero"]
            andPower:objectToProcess[@"powerOfHero"]
            andImageToDownloadLater:objectToProcess[@"imageOfHero"]];
}

@end
