//
//  ViewController.m
//  ParseTests
//
//  Created by Classroom on 10/23/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>
#import "Hero.h"
#import "APIManager.h"

@interface ViewController ()

@end

@implementation ViewController
{
    NSMutableArray *arrayOfHeroes;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self startDownloadOfHeroes];

}

#pragma mark TableView
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    Hero *heroForIndex = [arrayOfHeroes objectAtIndex:indexPath.row];
    cell.textLabel.text = heroForIndex.heroName;
    cell.detailTextLabel.text = heroForIndex.heroPower;
    
    if (heroForIndex.heroImage)
    {
        cell.imageView.image = heroForIndex.heroImage;
    }
    else
    {
        [APIManager retrieveImageForHero:heroForIndex];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayOfHeroes count];
}

#pragma mark API Calls
-(void)startDownloadOfHeroes
{
    arrayOfHeroes = [[NSMutableArray alloc] init];
    
    [self registerForNotificationOfImageDownloading];
    
    [APIManager retrieveObjectsOfCLass:@"Heroes" withSuccess:^(BOOL success, NSMutableArray *processedHeroes)
     {
         if (success)
         {
             arrayOfHeroes = processedHeroes;
             
             [self.tableView reloadData];
         }
     }];
}

//When image gets added to hero, refresh tableView
-(void)registerForNotificationOfImageDownloading
{
    [[NSNotificationCenter defaultCenter] addObserverForName:@"imageDownloaded" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note)
    {
        NSUInteger indexOfObject = [arrayOfHeroes indexOfObject:note.object];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexOfObject inSection:0];
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }];
}

@end
