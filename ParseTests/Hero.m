//
//  Hero.m
//  ParseTests
//
//  Created by Classroom on 10/26/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import "Hero.h"

@implementation Hero

+(Hero *)createHeroWithName:(NSString *)heroName andPower:(NSString *)heroPower andImageToDownloadLater:(PFFile *)imageToDownload
{
    Hero *newHero = [[Hero alloc] init];
    newHero.heroName = heroName;
    newHero.heroPower = heroPower;
    newHero.heroImageToDownload = imageToDownload;
    
    return newHero;
}

@end
