//
//  ViewController.h
//  ParseTests
//
//  Created by Classroom on 10/23/15.
//  Copyright © 2015 Classroom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

